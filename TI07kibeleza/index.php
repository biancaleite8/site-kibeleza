<!DOCTYPE html>
<html lang="pt-br"><!--idioma-->
<head>
    <meta charset='utf-8'><!--caracteres especiais-->
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Site Kibeleza</title><!--titulo do site-->
    <meta name='viewport' content='width=device-width, initial-scale=1'><!-- largura-->
    <link rel='stylesheet' type='text/css'  href='css/reset.css'>
    

    <!--Banner-->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>


    <link rel="stylesheet" type="text/css" href="css/lity.css">
    
    <link rel='stylesheet' type='text/css'  href='css/estilo.css'>

    <link rel='stylesheet' type='text/css'  href='css/anima.css'>
    
</head>
<body> 


<!------AQUI ABAIXO ESTÁ O TOPO------->

    
    <?php require_once("topo.php") ?>
    <?php require_once("banner.php") ?>
    


    


             <!-------  SOBRE  ------->

    <section class="site corpoSobre animate__animated animate__bounceInUp">
        <article>
            <h2>Sobre</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.  </p>
        </article>

        <article>
            <img src="img/local.png" alt="video sobre">
        </article>
    </section><!-- fim sobre-->


    <?php require_once("instagram.php") ?>

    <!-----------SERVIÇOS----------->


    <div class="faixaServico">
        <section class="site corposervico animate__animated animate__bounceInUp">
            <h2>Serviços</h2>
            
            <?php require_once("lista-servico.php") ?>

        </section>
    </div><!--fim serviços-->


             <!-------  DESTAQUE  ------->

    <section class="destaque animate__animated animate__backInUp">
        <a href="//www.youtube.com/watch?v=XSGBVzeBUbk" data-lity>
            <img src="img/icon/play.svg" alt="player video"/>
        </a>      
    </section><!-- fim destaque-->


             <!-------  EQUIPE  ------->


    <section class="site corpoEquipe animate__animated animate__bounceIn"> 
        <h2>Equipe</h2>
        <?php require_once("equipe.php") ?>
    </section>

             <!-------  BLOG  ------->

   
    <div class="faixaBlog">
        <h2>Blog</h2>
        <section class="blog site animate__animated animate__ fadeIn">
            
            <article>
                <img src="img/blog1.png" alt="blog">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </article>
            <article>
                <img src="img/blog2.png" alt="blog">
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
            </article>
        </section>
    </div><!--fim blog-->
        

             <!-------  RODAPÉ  ------->

             <?php require_once("rodape.php") ?>
             

    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/slick.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/lity.js"></script>
    <script type="text/javascript" src="js/minhasAnimacoes.js"></script>


</body>
</html>