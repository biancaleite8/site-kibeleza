<!DOCTYPE html>
<html lang="pt-br"><!--idioma-->
<head>
    <meta charset='utf-8'><!--caracteres especiais-->
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <title>Site Kibeleza</title><!--titulo do site-->
    <meta name='viewport' content='width=device-width, initial-scale=1'><!-- largura-->
    <link rel='stylesheet' type='text/css'  href='css/reset.css'>
    

    <!--Banner-->
    <link rel="stylesheet" type="text/css" href="css/slick.css">
    <link rel="stylesheet" type="text/css" href="css/slick-theme.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>


    <link rel="stylesheet" type="text/css" href="css/lity.css">
    
    <link rel='stylesheet' type='text/css'  href='css/estilo.css'>

    <link rel='stylesheet' type='text/css'  href='css/anima.css'>
    
</head>
<body> 


    
    <?php require_once("topo.php") ?>
    <?php require_once("banner.php") ?>
    

    <!-----------SERVIÇOS----------->


            <section class="site corposervico animate__animated animate__bounceInUp">
            <h2>Serviços</h2>
            
            <?php require_once("lista-servico.php") ?>

        </section>
    <!--fim serviços-->
    

    <?php require_once("instagram.php") ?>



             <!-------  DESTAQUE  ------->

    <section class="destaque animate__animated animate__backInUp">
        <a href="//www.youtube.com/watch?v=XSGBVzeBUbk" data-lity>
            <img src="img/icon/play.svg" alt="player video"/>
        </a>      
    </section><!-- fim destaque-->



        

             <!-------  RODAPÉ  ------->

             <?php require_once("rodape.php") ?>
             

    <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
    <script type="text/javascript" src="js/slick.js"></script>
    <script type="text/javascript" src="js/wow.js"></script>
    <script type="text/javascript" src="js/lity.js"></script>
    <script type="text/javascript" src="js/minhasAnimacoes.js"></script>


</body>
</html>