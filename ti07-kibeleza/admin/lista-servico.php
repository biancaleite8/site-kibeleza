<?php

require_once("conexao.php");

try{

    $conexao = Conexao::LigarConexao();
    $conexao->exec("SET NAMES utf8");

if (!$conexao){
    echo "Não está conectado ao banco";


}

$query = $conexao->prepare("SELECT * FROM servico ORDER BY nomeServico");

$query->execute();

$json = "[";


   
    while($resultado = $query->fetch()){

        if ($json != "["){
            $json .= ",";

        }

        $json .= '{"Codigo":             "'.$resultado["idServico"].'",';            
        $json .= '"Nome":                "'.$resultado["nomeServico"].'",';
        $json .= '"Valor":               "'.$resultado["valorServico"].'",';
        $json .= '"Status":              "'.$resultado["statusServico"].'",';       
        $json .= '"dataCadServico":      "'.$resultado["dataCadServico"].'",';   
        $json .= '"Foto":                "'.$resultado["fotoServico"].'",';
        $json .= '"Foto1":               "'.$resultado["foto1Servico"].'",';
        $json .= '"Foto2":               "'.$resultado["foto2Servico"].'",';
        $json .= '"Foto3":               "'.$resultado["foto3Servico"].'",';
        $json .= '"Descricao":           "'.$resultado["descServico"].'",';  
        $json .= '"Texto":               "'.$resultado["texto"].'",';    
        $json .= '"Tempo":               "'.$resultado["tempoServico"].'"}';
  
    }
    
    $json .= "]";

    echo $json;

}catch(Exception $e){
    echo "Erro ". $e->getMessage();
    
}



?>
